<?php

class AA{

	public function __construct($a)
	{
		$this->z = $a;
	}

	public function www(): string {
		return $this->z;
	}
}
class AX{
	public function __construct($aaa, $bb)
	{
	}

	public function www($a){
		return $a;
	}
}
$g = $_GET['input'];
echo (new AA($g))->www(); //  this cause the printer to print operand
$x = new AA($g);
echo $x->www();
echo (new AX(1,2))->www($g);
